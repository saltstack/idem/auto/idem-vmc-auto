def __init__(hub):
    # This enables acct profiles that begin with "csp" or "vmc" for vmc modules
    hub.exec.vmc.ACCT = ["csp", "vmc"]
