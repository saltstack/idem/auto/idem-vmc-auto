

def __init__(hub):
    hub.exec.vmc.ENDPOINT_URLS = ['https://vmc.vmware.com/vmc/api']
    # The default is the first in the list
    hub.exec.vmc.DEFAULT_ENDPOINT_URL = "https://vmc.vmware.com/vmc/api"

    # This enables acct profiles that begin with "csp" or "vmc" for vmc modules
    hub.exec.vmc.ACCT = ["csp", "vmc"]

    def _get_version_sub(ctx, *args, **kwargs):
        api_version = ctx.acct.get("api_version", "latest")
        return hub.exec.vmc[api_version]

    # Get the version sub dynamically from the ctx variable/acct
    hub.pop.sub.dynamic(hub.exec.vmc, _get_version_sub)
