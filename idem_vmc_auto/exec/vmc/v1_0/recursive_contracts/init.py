from pop.contract import ContractedContext


def pre(hub, ctx: ContractedContext):
    """
    If the endpoint_url wasn't specified in the func_ctx, then supply a default
    """
    func_ctx = ctx.get_argument("ctx")
    if "endpoint_url" not in func_ctx.acct:
        func_ctx.acct.endpoint_url = hub.exec.vmc.DEFAULT_ENDPOINT_URL
