"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def orgs(hub, ctx) -> None:
    """
    **Autogenerated function**
    
    Return a list of all organizations the calling user (based on credential) is authorized on.


    Returns:
        None

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.vmc.orgs.orgs(ctx, )

        Call from CLI:

        .. code-block:: bash

            idem exec vmc.orgs.orgs 
    """

    return await hub.tool.http.session.request(
        ctx,
        method="get",
        path=ctx.acct.endpoint_url + "/orgs".format(**{}),
        query_params={},
        data={}
    )


async def providers(hub, ctx, org: str) -> None:
    """
    **Autogenerated function**
    
    Args:
        org(str): Organization identifier.

    Returns:
        None

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.vmc.orgs.providers(ctx, org=value)

        Call from CLI:

        .. code-block:: bash

            idem exec vmc.orgs.providers org=value
    """

    return await hub.tool.http.session.request(
        ctx,
        method="get",
        path=ctx.acct.endpoint_url + "/orgs/{org}/providers".format(**{"org": org}),
        query_params={},
        data={}
    )

