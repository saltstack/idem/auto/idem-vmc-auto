Idem-Vmc-Auto
=============

vmc Cloud Provider for Idem

DEVELOPMENT
===========

Clone the `idem-vmc-auto` repository and install with pip.

.. code:: bash

    git clone git@gitlab.com:my-user/idem-vmc-auto.git
    pip install -e idem-vmc-auto

ACCT
====

After installation vmc Idem Provider execution and state modules will be accessible to the pop `hub`.
In order to use them we need to set up our credentials.

Create a new file called `credentials.yaml` and populate it with profiles.
The `default` profile will be used automatically by `idem` unless you specify one with `--acct-profile=profile_name` on the cli.

`acct backends <https://gitlab.com/saltstack/pop/acct-backends>`_ provide alternate methods for storing profiles.

The vmc provider collects parameters for "idem-aiohttp" to use for authentication.
A profile needs to specify the authentication parameters for vmc.
Every one of the parameters is optional except for session.headers.csp-refresh-token.
Here, all available options are shown with their defaults:
# TODO idem-csp-auto needs to be created with an acct plugin that converts API tokens to access_tokens.
#     -- this needs to be another way to authenticate to idem-vmc

credentials.yaml

..  code:: sls

    vmc:
      default:
        endpoint_urL: https://vmc.vmware.com/vmc/api
        auth:
          # aiohttp.BasicAuth options
          login:
          password:
          encoding: latin1
        connector:
          # aiohttp.connector.TCPConnector options
          verify_ssl: True,
          fingerprint:
          use_dns_cache: True
          ttl_dns_cache: 10
          family: 0
          ssl_context:
          ssl:
          local_addr:
          keepalive_timeout:
          force_close: False
          limit: 100
          limit_per_host: 0
          enable_cleanup_closed: False
        resolver:
          # aiodns.DNSResolver options
          nameservers:
          # pycares.Channel options
          flags:
          timeout:
          tries:
          ndots:
          tcp_port:
          udp_port:
          servers:
          domains:
          lookups:
          sock_state_cb:
          socket_send_buffer_size:
          socket_receive_buffer_size:
          rotate:
          local_ip:
          local_dev:
          resolvconf_path:
        session:
          # aiohttp.ClientSession options
          cookies:
          headers:
            csp-refresh-token: < access_token_value >
          skip_auto_headers:
          version: http_version
          connector_owner: True
          raise_for_status: False
          conn_timeout:
          auto_decompress: True
          trust_env: False
          requote_redirect_url: True
          trace_configs:
          read_bufsize: 65536

Now encrypt the credentials file and add the encryption key and encrypted file path to the ENVIRONMENT.

The `acct` command should be available as it is a requisite of `idem` and `idem_vmc_auto`.
Encrypt the the credential file.

.. code:: bash

    acct encrypt credentials.yaml

output::

    -A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI=

Add these to your environment:

.. code:: bash

    export ACCT_KEY="-A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI="
    export ACCT_FILE=$PWD/credentials.yaml.fernet


USAGE
=====
A profile can be specified for use with a specific state.
If no profile is specified, the profile called "default", if one exists, will be used:

.. code:: sls

    ensure_user_exists:
      vmc.user.present:
        - acct_profile: my-staging-env
        - name: a_user_name
        - kwarg1: val1

It can also be specified from the command line when executing states.

.. code:: bash

    idem state --acct-profile my-staging-env my_state.sls

It can also be specified from the command line when calling an exec module directly.

.. code:: bash

    idem exec --acct-profile my-staging-env vmc.user.list


AUTOGENERATION
==============

Perhaps you want to run the autogeneration manually.
Make sure to include the config from the build.conf in the root of idem-vmc-auto.
It includes common parameters (such as which files to ignore when overwriting).
Go to `the vmc developer console <https://vmc.vmware.com/console/developer/api-explorer/general>`_ and download the api spec for vmc.
Save it to your downloads folder as "general.json"

.. code:: bash

    $ cd /root/of/idem-vmc-auto
    $ pip install pop-create-idem
    $ pop-create swagger -c build.conf --spec ~/Downloads/general.json